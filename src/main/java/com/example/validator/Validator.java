package com.example.validator;

public interface Validator<T> {
  boolean isValid(T t);
}
