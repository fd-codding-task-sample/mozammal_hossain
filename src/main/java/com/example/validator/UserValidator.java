package com.example.validator;

import com.example.exception.AgeZeroOrNegativeException;
import com.example.exception.UserNullOrNameNullOrEmptyException;
import com.example.model.User;

public class UserValidator implements Validator<User> {

  @Override
  public boolean isValid(final User user) {
    if (user == null) {
      throw new UserNullOrNameNullOrEmptyException("user or name cant be null or empty");
    }
    String name = user.getName();
    if (name == null || name.trim().equals("")) {
      throw new UserNullOrNameNullOrEmptyException("user or name cant be null or empty");
    }

    if (user.getAge() <= 0) {
      throw new AgeZeroOrNegativeException("user or name cant be null or empty");
    }

    return true;
  }
}
