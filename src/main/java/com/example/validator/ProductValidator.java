package com.example.validator;

import com.example.exception.ProductNullOrProductIdNullOrEmptyException;
import com.example.model.Product;

public class ProductValidator implements Validator<Product> {

  @Override
  public boolean isValid(final Product product) {
    if (product == null) {
      throw new ProductNullOrProductIdNullOrEmptyException(
          "product or product id cant be null or empty");
    }

    String productId = product.getId();
    if (productId == null || productId.trim().equals("")) {
      throw new ProductNullOrProductIdNullOrEmptyException(
          "product or product id cant be null or empty");
    }
    return true;
  }
}
