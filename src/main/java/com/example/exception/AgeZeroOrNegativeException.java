package com.example.exception;

public class AgeZeroOrNegativeException extends BaseException {

  public AgeZeroOrNegativeException(String message) {
    super(message);
  }
}
