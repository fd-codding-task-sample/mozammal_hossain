package com.example.exception;

public class ProductNullOrProductIdNullOrEmptyException extends BaseException {

  public ProductNullOrProductIdNullOrEmptyException(String message) {
    super(message);
  }
}
