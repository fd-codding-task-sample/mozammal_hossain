package com.example.exception;

public class UserNullOrNameNullOrEmptyException extends BaseException {

  public UserNullOrNameNullOrEmptyException(String message) {
    super(message);
  }
}
