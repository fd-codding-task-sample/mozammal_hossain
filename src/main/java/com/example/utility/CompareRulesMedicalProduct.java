package com.example.utility;

import com.example.model.UserWithRank;
import com.example.utility.CompareRuleHelper;

import java.util.Comparator;

public class CompareRulesMedicalProduct implements Comparator<UserWithRank> {

  @Override
  public int compare(final UserWithRank o1, final UserWithRank o2) {
    if (o1.getUser().isPremium() && o2.getUser().isPremium()) {
      return CompareRuleHelper.compareByAge(o1, o2);
    } else if (o1.getUser().isPremium()) {
      return -1;
    } else if (o2.getUser().isPremium()) {
      return 1;
    } else {
      return CompareRuleHelper.compareByAge(o1, o2);
    }
  }
}
