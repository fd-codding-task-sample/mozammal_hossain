package com.example.utility;

import com.example.model.UserWithRank;

public class CompareRuleHelper {

  private CompareRuleHelper() {}

  public static int compareByAge(final UserWithRank o1, final UserWithRank o2) {
    if (o1.getUser().getAge() > 70 && o2.getUser().getAge() <= 70) {
      return -1;
    } else if (o1.getUser().getAge() <= 70 && o2.getUser().getAge() > 70) {
      return 1;
    } else {
      return Integer.compare(o1.getTimestamp(), o2.getTimestamp());
    }
  }
}
