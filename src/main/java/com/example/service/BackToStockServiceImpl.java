package com.example.service;

import com.example.model.Product;
import com.example.model.ProductCategory;
import com.example.model.User;
import com.example.model.UserWithRank;
import com.example.validator.Validator;

import java.util.*;

public class BackToStockServiceImpl implements BackToStockService {

  private static final int DEFAULT_INITIAL_CAPACITY = 16;

  private static int timestamp = 0;

  private final Map<Product, PriorityQueue<UserWithRank>> rankTree;

  private final Comparator<UserWithRank> compareRulesMedicalProduct;

  private final Comparator<UserWithRank> compareRulesNonMedicalProduct;

  private final Validator<Product> productValidator;

  private final Validator<User> userValidator;

  public BackToStockServiceImpl(
      Map<Product, PriorityQueue<UserWithRank>> rankTree,
      Comparator<UserWithRank> compareRulesMedicalProduct,
      Comparator<UserWithRank> compareRulesNonMedicalProduct,
      Validator<Product> productValidator,
      Validator<User> userValidator) {
    this.rankTree = rankTree;
    this.compareRulesMedicalProduct = compareRulesMedicalProduct;
    this.compareRulesNonMedicalProduct = compareRulesNonMedicalProduct;
    this.userValidator = userValidator;
    this.productValidator = productValidator;
  }

  @Override
  public void subscribe(final User user, final Product product) {
    if (productValidator.isValid(product)
        && userValidator.isValid(user)
        && !rankTree.containsKey(product)) {
      if (product.getCategory() == ProductCategory.MEDICAL) {
        rankTree.put(
            product, new PriorityQueue<>(DEFAULT_INITIAL_CAPACITY, compareRulesMedicalProduct));
      } else {
        rankTree.put(
            product, new PriorityQueue<>(DEFAULT_INITIAL_CAPACITY, compareRulesNonMedicalProduct));
      }
    }

    rankTree.get(product).add(new UserWithRank(timestamp, user));
    timestamp++;
  }

  @Override
  public List<User> subscribedUsers(final Product product) {
    List<User> users = new ArrayList<>();

    if (rankTree.containsKey(product)) {
      PriorityQueue<UserWithRank> userWithRanks = rankTree.get(product);

      while (!userWithRanks.isEmpty()) {
        users.add(userWithRanks.remove().getUser());
      }
    }

    return users;
  }
}
