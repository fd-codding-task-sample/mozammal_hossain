package com.example.model;

public class UserWithRank {

  private final Integer timestamp;

  private final User user;

  public UserWithRank(Integer timestamp, User user) {
    this.timestamp = timestamp;
    this.user = user;
  }

  public Integer getTimestamp() {
    return timestamp;
  }

  public User getUser() {
    return user;
  }

  @Override
  public String toString() {
    return "UserWithRank{" + "timestamp=" + timestamp + ", user=" + user + '}';
  }
}
