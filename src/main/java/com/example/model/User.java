package com.example.model;

import java.util.Objects;

public class User {
  private final String name;
  private final boolean premium;
  private final int age;

  public User(String name, boolean premium, int age) {
    this.name = name;
    this.premium = premium;
    this.age = age;
  }

  public String getName() {
    return name;
  }

  public boolean isPremium() {
    return premium;
  }

  public int getAge() {
    return age;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    User user = (User) o;
    return name.equals(user.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name);
  }

  @Override
  public String toString() {
    return "User{" + "name='" + name + '\'' + ", premium=" + premium + ", age=" + age + '}';
  }
}
