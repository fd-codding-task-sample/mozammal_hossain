package com.example.view;

import com.example.model.User;

import java.util.List;

public class ConsoleDisplayView implements DisplayView<User> {

  public ConsoleDisplayView() {}

  @Override
  public void display(final List<User> users) {
    System.out.println("START DISPLAYING USERS BY PRIORITY RULES");
    System.out.println("------------------------------------");
    System.out.println(users);
    System.out.println("END");
    System.out.println();
  }
}
