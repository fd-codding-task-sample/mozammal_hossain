package com.example.view;

import java.util.List;

public interface DisplayView<U> {

  void display(final List<U> u);
}
