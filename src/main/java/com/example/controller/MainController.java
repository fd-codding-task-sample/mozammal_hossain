package com.example.controller;

import com.example.exception.ExceptionHandler;
import com.example.model.Product;
import com.example.model.ProductCategory;
import com.example.model.User;
import com.example.model.UserWithRank;
import com.example.service.BackToStockService;
import com.example.service.BackToStockServiceImpl;
import com.example.utility.CompareRulesMedicalProduct;
import com.example.utility.CompareRulesNonMedicalProduct;
import com.example.validator.ProductValidator;
import com.example.validator.UserValidator;
import com.example.view.ConsoleDisplayView;
import com.example.view.DisplayView;

import java.util.*;

public class MainController {

  private final BackToStockService backToStockService;
  private final DisplayView<User> displayView;

  public MainController(
      final BackToStockService backToStockService, final DisplayView<User> displayView) {
    this.backToStockService = backToStockService;
    this.displayView = displayView;
  }

  public void renderOnConsoleView(List<User> items) {
    displayView.display(items);
  }

  public void runApp() {
    final Product product1 = new Product("1", ProductCategory.BOOKS);
    final Product product2 = new Product("2", ProductCategory.MEDICAL);

    final User user1 = new User("nasa", false, 50);
    final User user2 = new User("mouri", true, 12);
    final User user3 = new User("manwara", false, 72);
    final User user4 = new User("janant", false, 14);
    final User user5 = new User("karim", false, 76);
    final User user6 = new User("rahim", true, 79);

    backToStockService.subscribe(user1, product1);
    backToStockService.subscribe(user1, product2);
    backToStockService.subscribe(user2, product1);
    backToStockService.subscribe(user2, product2);

    backToStockService.subscribe(user3, product2);
    backToStockService.subscribe(user4, product2);
    backToStockService.subscribe(user4, product1);
    backToStockService.subscribe(user5, product2);

    backToStockService.subscribe(user3, product1);
    backToStockService.subscribe(user6, product1);
    backToStockService.subscribe(user6, product2);

    renderOnConsoleView(backToStockService.subscribedUsers(product1));
    renderOnConsoleView(backToStockService.subscribedUsers(product2));
  }

  public static void main(String[] args) {
    Thread.currentThread().setUncaughtExceptionHandler(new ExceptionHandler());

    final Map<Product, PriorityQueue<UserWithRank>> rankTree = new HashMap<>();
    final MainController controller =
        new MainController(
            new BackToStockServiceImpl(
                rankTree,
                new CompareRulesMedicalProduct(),
                new CompareRulesNonMedicalProduct(),
                new ProductValidator(),
                new UserValidator()),
            new ConsoleDisplayView());

    controller.runApp();
  }
}
