package com.example;

import static org.junit.jupiter.api.Assertions.*;

import com.example.exception.ProductNullOrProductIdNullOrEmptyException;
import com.example.exception.UserNullOrNameNullOrEmptyException;
import com.example.model.Product;
import com.example.model.ProductCategory;
import com.example.model.User;
import com.example.model.UserWithRank;
import com.example.service.BackToStockServiceImpl;
import com.example.utility.CompareRulesMedicalProduct;
import com.example.utility.CompareRulesNonMedicalProduct;
import com.example.validator.ProductValidator;
import com.example.validator.UserValidator;
import com.example.validator.Validator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

public class BackToStockServiceImplTest {

  private BackToStockServiceImpl backToStockService;

  private Map<Product, PriorityQueue<UserWithRank>> rankTree;

  private Comparator<UserWithRank> compareRulesMedicalProduct;

  private Comparator<UserWithRank> compareRulesNonMedicalProduct;

  private Validator<Product> productValidator;

  private Validator<User> userValidator;

  @BeforeEach
  public void init() {
    rankTree = new HashMap<>();
    compareRulesMedicalProduct = new CompareRulesMedicalProduct();
    compareRulesNonMedicalProduct = new CompareRulesNonMedicalProduct();
    productValidator = new ProductValidator();
    userValidator = new UserValidator();
    backToStockService =
        new BackToStockServiceImpl(
            rankTree,
            compareRulesMedicalProduct,
            compareRulesNonMedicalProduct,
            productValidator,
            userValidator);
  }

  @Test
  public void givenUsersSubscribeToProductMapShouldBeNonEmpty() {
    Product product1 = new Product("1", ProductCategory.BOOKS);
    Product product2 = new Product("2", ProductCategory.MEDICAL);

    User user1 = new User("moz", false, 50);
    User user2 = new User("mouri", true, 12);

    backToStockService.subscribe(user1, product1);
    backToStockService.subscribe(user1, product2);
    backToStockService.subscribe(user2, product1);
    backToStockService.subscribe(user2, product2);

    assertEquals(2, rankTree.size());
    assertEquals(2, backToStockService.subscribedUsers(product1).size());
    assertEquals(2, backToStockService.subscribedUsers(product2).size());
  }

  @Test
  public void givenNoneSubscribeToProductMapShouldBeEmpty() {
    assertEquals(0, rankTree.size());
  }

  @Test
  public void
      givenAllTypeOfUsersSubscribeToProductUserShouldNotifiedByPremiumFirstOlderPeopleSecondYoungPeopleLastWithFIFO() {
    Product product1 = new Product("1", ProductCategory.BOOKS);
    Product product2 = new Product("2", ProductCategory.MEDICAL);

    User user1 = new User("nasa", false, 50);
    User user2 = new User("mouri", true, 12);
    User user3 = new User("manwara", false, 72);
    User user4 = new User("janant", false, 14);
    User user5 = new User("karim", false, 76);

    backToStockService.subscribe(user1, product1);
    backToStockService.subscribe(user1, product2);
    backToStockService.subscribe(user2, product1);

    backToStockService.subscribe(user3, product2);
    backToStockService.subscribe(user4, product2);
    backToStockService.subscribe(user4, product1);
    backToStockService.subscribe(user5, product2);
    backToStockService.subscribe(user3, product1);

    List<User> actual = backToStockService.subscribedUsers(product1);
    assertIterableEquals(actual, Arrays.asList(user2, user3, user1, user4));

    actual = backToStockService.subscribedUsers(product2);
    assertIterableEquals(actual, Arrays.asList(user3, user5, user1, user4));
  }

  @Test
  public void
      givenOldAndYoungNonPremiumUsersSubscribeToMedicalProductOlderUserShouldNotifiedFirst() {
    Product product2 = new Product("2", ProductCategory.MEDICAL);

    User user1 = new User("nasa", false, 50);
    User user2 = new User("mouri", false, 12);
    User user3 = new User("manwara", false, 56);
    User user4 = new User("janant", false, 14);
    User user5 = new User("karim", false, 76);

    backToStockService.subscribe(user1, product2);
    backToStockService.subscribe(user2, product2);
    backToStockService.subscribe(user3, product2);
    backToStockService.subscribe(user4, product2);
    backToStockService.subscribe(user5, product2);

    List<User> actual = backToStockService.subscribedUsers(product2);
    assertIterableEquals(actual, Arrays.asList(user5, user1, user2, user3, user4));
  }

  @Test
  public void
      givenOldPremiumNonPremiumAndYoungPremiumUsersSubscribeToMedicalProductOlderUserShouldNotifiedFirstThenPremiumThenNonPremiumOldUsersThenNonPremiumYoungByFIFO() {
    Product product2 = new Product("2", ProductCategory.MEDICAL);

    User user1 = new User("nasa", false, 50);
    User user2 = new User("mouri", true, 12);
    User user3 = new User("manwara", false, 72);
    User user4 = new User("janant", false, 14);
    User user5 = new User("karim", false, 76);
    User user6 = new User("rahim", true, 79);

    final Map<Product, List<User>> subscribers = new HashMap<>();
    final Map<Product, PriorityQueue<UserWithRank>> rankTree = new HashMap<>();

    backToStockService.subscribe(user1, product2);
    backToStockService.subscribe(user2, product2);

    backToStockService.subscribe(user3, product2);
    backToStockService.subscribe(user4, product2);
    backToStockService.subscribe(user5, product2);

    backToStockService.subscribe(user6, product2);

    List<User> actual = backToStockService.subscribedUsers(product2);
    assertIterableEquals(actual, Arrays.asList(user6, user2, user3, user5, user1, user4));
  }

  @Test
  public void givenAllNonPremiumUsersSubscribeToProductUserShouldNotifiedByFIFO() {
    Product product1 = new Product("1", ProductCategory.BOOKS);
    Product product2 = new Product("2", ProductCategory.DIGITAL);

    User user1 = new User("nasa", false, 50);
    User user2 = new User("mouri", false, 12);
    User user3 = new User("manwara", false, 12);
    User user4 = new User("janant", false, 14);
    User user5 = new User("karim", false, 33);
    User user6 = new User("rahim", false, 71);

    backToStockService.subscribe(user1, product2);
    backToStockService.subscribe(user2, product1);
    backToStockService.subscribe(user2, product2);
    backToStockService.subscribe(user1, product1);

    backToStockService.subscribe(user4, product2);
    backToStockService.subscribe(user3, product2);
    backToStockService.subscribe(user4, product1);
    backToStockService.subscribe(user5, product2);

    backToStockService.subscribe(user3, product1);
    backToStockService.subscribe(user6, product1);
    backToStockService.subscribe(user6, product2);

    List<User> actual = backToStockService.subscribedUsers(product1);
    assertIterableEquals(actual, Arrays.asList(user6, user2, user1, user4, user3));

    actual = backToStockService.subscribedUsers(product2);
    assertIterableEquals(actual, Arrays.asList(user6, user1, user2, user4, user3, user5));
  }

  @Test
  public void givenNoUsersSubscribeToProductNotificationShouldReturnNull() {
    Product product = new Product("1", ProductCategory.DIGITAL);

    List<User> actual = backToStockService.subscribedUsers(product);
    assertTrue(actual.isEmpty());
  }

  @Test
  public void givenInvalidUserThrowException() {
    Product product = new Product("1", ProductCategory.DIGITAL);
    UserNullOrNameNullOrEmptyException exception =
        assertThrows(
            UserNullOrNameNullOrEmptyException.class,
            () -> backToStockService.subscribe(null, product),
            "Expected UserNullOrNameNullOrEmptyException to throw but not happened");
  }

  @Test
  public void givenInvalidProductThrowException() {
    Product product = new Product(null, ProductCategory.DIGITAL);
    User user = new User("nasa", false, 50);

    ProductNullOrProductIdNullOrEmptyException exception =
        assertThrows(
            ProductNullOrProductIdNullOrEmptyException.class,
            () -> backToStockService.subscribe(user, product),
            "Expected ProductNullOrProductIdNullOrEmptyException to throw but not happened");
  }
}
