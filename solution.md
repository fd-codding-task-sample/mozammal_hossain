Task (BackToStock)

## Testing Locally

```shell
gradle test
```

### Time Complexity
1. Both subscribe  and subscribedUsers take O(n*log(n)) where n is the total number of users

### Good Programming Practices and Data Structure used in this Application

1. Program to an interface, not and implementation.

2. Favor composition most of the time over inheritance.

3. Design pattern such MVC.

4. Unit Test

5. Exception handling

5. Priority Queue
 
